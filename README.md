Holds QA utilities that don't have a place in other repos.

react-app -- Presents build manifest data served up by rest-server as a web page
rest-server - Serves up the manifest data via a rest api used by react-app:

To run the manifest web page helper you'll need two windows

Running the rest api server on port 8000

 cd rest-server
 copy checkin report here as filename 'Checkin_Report.csv'
 ./server


Running the react web app on port 3000

 cd react-app
 npm install
 npm start

Open web page to port 3000.  Or it may be opened for you.


