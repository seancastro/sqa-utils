import React, { Component } from "react";
import Services from "./components/Services";
import Commits from "./components/Commits";
import CommitBanner from "./components/layout/CommitBanner";
import { Header } from "./components/layout/HeaderBanner";
import { Container, Row, Col } from "reactstrap";
import "./App.css";

class App extends Component {
  state = {
    loaded: null,
    service_name: null,
    services: {},
    jira_map: ["CCP", "INSP", "SSOT", "DEVOP"]
  };

  componentDidMount() {
    this.setState({ loaded: false });
    var url =
      "http://localhost:8000/services?filename=Checkin_Report.csv&context=tagged";
    fetch(url)
      .then(d => d.json())
      .then(d => {
        this.setState({ services: d });
        return this;
      });
    this.setState({ loaded: true });
  }

  // Load up commits
  clickItem = name => {
    this.setState({ loaded: false });
    this.setState({ service_name: name });
    this.setState({ loaded: true });
  };

  delItem = name => {
    this.setState({
      services: [
        ...this.state.services.filter(service => service.name !== name)
      ]
    });
  };

  render() {
    if (!this.state.loaded) {
      return <p>Loading...</p>;
    }
    return (
      <div className="App">
        <Container>
          <Row>
            <Col>
              <Header />
              <Services
                services={this.state.services}
                clickItem={this.clickItem}
                delItem={this.delItem}
              />
            </Col>
            <Col>
              <CommitBanner
                services={this.state.services}
                service_name={this.state.service_name}
              />
              <Commits
                jira_map={this.state.jira_map}
                services={this.state.services}
                service_name={this.state.service_name}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
