import React, { Component } from "react";

const divStyle = {
  background: "#f4f4f4",
  padding: "5px 5px 5px 5px",
  borderBottom: "1px #ccc dotted",
  textDecoration: "none"
};

const btnStyle = {
  background: "#DDD",
  color: "#000",
  border: "none",
  padding: "0px 5px",
  cursor: "pointer",
  float: "right"
};

class Commit extends Component {
  renderCommits = () => {
    let jira_tickets = [];
    let ticket_line = "";
    let tag_line = "";
    const { commit, jira_map, index } = this.props;
    if (!commit || !jira_map) {
      return "";
    }
    let { files, c_hash, c_branch, c_desc, c_tag, c_date, c_author } = commit;
    // Annoying compiler warnings
    files = c_branch;
    c_branch = files;
    //

    for (let ticket in jira_map) {
      let start_index = c_desc.indexOf(jira_map[ticket]);
      if (start_index >= 0) {
        let end_index = c_desc.indexOf(" ", start_index);
        if (end_index === -1) {
          end_index = c_desc.length;
        }
        jira_tickets.push(c_desc.substring(start_index, end_index));
      }
    }
    ticket_line = jira_tickets.map(ticket => {
      let href_line = "https://clearcapital.atlassian.net/browse/" + ticket;
      return (
        <div key={ticket}>
          <a style={{ fontWeight: "bold" }}>TICKET: </a>
          <a target="new" href={href_line} style={{ fontWeight: "bold" }}>
            {ticket}
          </a>
          <br />
        </div>
      );
    });

    if (c_tag) {
      tag_line = (
        <div>
          <a style={{ fontWeight: "bold" }}>Tag: {c_tag}</a>
          <br />
        </div>
      );
    }

    return (
      <div key={c_hash} style={divStyle}>
        <button style={btnStyle}>{index}</button>
        {ticket_line}
        {tag_line}
        <a>Date: {c_date}</a>
        <br />
        <a>Author: {c_author}</a>
        <br />
        <a>Hash: {c_hash}</a>
        <br />
        <a>Desc: {c_desc}</a>
        <br />
      </div>
    );
  };
  render() {
    return this.renderCommits();
  }
}

export default Commit;
