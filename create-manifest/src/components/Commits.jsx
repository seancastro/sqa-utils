import React, { Component } from "react";
import Commit from "./Commit";

class Commits extends Component {
  render() {
    if (!this.props.service_name) {
      return <h5>No service selected</h5>;
    }
    let services = this.props.services.services;
    let commits = services[this.props.service_name]["commits"];

    let index = 0;
    return commits.map(commit => {
      index += 1;
      return (
        <Commit
          index={index}
          jira_map={this.props.jira_map}
          key={commit.c_hash}
          commit={commit}
        />
      );
    });
  }
}

export default Commits;
