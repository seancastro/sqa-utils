import React, { Component } from "react";

const divStyle = {
  background: "#f4f4f4",
  padding: "5px 5px 5px 5px",
  borderBottom: "1px #ccc dotted",
  textDecoration: "none"
};

function format_version(item) {
  return item.commits.length + " commits";
}

class ServiceItem extends Component {
  render() {
    const { name } = this.props.item;
    return (
      <div
        key={name}
        style={divStyle}
        onClick={this.props.clickItem.bind(this, name)}
      >
        <a>
          {name} - {format_version(this.props.item)}
        </a>
      </div>
    );
  }
}

export default ServiceItem;
