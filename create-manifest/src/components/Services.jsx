import React, { Component } from "react";
import ServiceItem from "./ServiceItem";

class Services extends Component {
  render() {
    let services = this.props.services;
    if (!services || !services.services) {
      return <h3>Loading...</h3>;
    }
    return Object.keys(services.services).map(item => {
      let item_obj = services.services[item];
      return (
        <ServiceItem
          key={item_obj.name}
          item={item_obj}
          delItem={this.props.delItem}
          clickItem={this.props.clickItem}
        />
      );
    });
  }
}

export default Services;
