import React, { Component } from "react";

const commitStyle = {
  background: "#333",
  color: "#fff",
  textAlign: "center",
  padding: "10px"
};

class CommitBanner extends Component {
  renderCommit = () => {
    let services = null;
    if (!this.props.services["services"] || !this.props.service_name) {
      return "";
    } else {
      services = this.props.services.services;
    }
    let commits = services[this.props.service_name]["commits"];
    return commits.length > 0 ? commits.length : "";
  };

  render() {
    return (
      <header style={commitStyle}>
        <h2>
          {this.renderCommit()} {this.props.service_name} Commits
        </h2>
      </header>
    );
  }
}

export default CommitBanner;
