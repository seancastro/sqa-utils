import React from "react";

const headerStyle = {
  background: "#333",
  color: "#fff",
  textAlign: "center",
  padding: "10px"
};

function Header() {
  return (
    <header
      style={headerStyle}
      onClick={() => {
        this.setState({ loaded: true });
      }}
    >
      <h2>Service Updates</h2>
    </header>
  );
}

export { Header };
