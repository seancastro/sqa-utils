#!/usr/bin/pythono

import sys
sys.path.append('src/python/libs/')
import json
import requests
import time
import argparse
import traceback

from Utils import Print
from Logger import Logger
import Utils

'''
Interfaces with AWS to gather the pod instances and verify that they are the expected version and roll over in a
reasonable amount of time.  This script assumes that a version comparison validation has already been done.
'''

def ScrubService(service):
    return '-'.join(service.split('-')[0:-2])

class PodStability(object):

    def __init__(self, args):
        self.args = args
        # From Instancely
        self.instance_versions = {}
        # From kube config yamls
        self.kube_yamls = {}
        self.csv_versions = {}
        self.logger = Logger.get_instance()

    def LoadInstances(self):
        try:
            resp = requests.get('https://instancely.clearcollateral.com/instance_data.json')
            clusters_json = resp.json()

            for cluster in clusters_json['data']:
                self.instance_versions[cluster] = {}
                if (cluster.find('ccp-') > -1):
                    services = clusters_json['data'][cluster]
                    for service in services:
                        pod_obj = service[service.keys()[0]]
                        pod_name = pod_obj['name']

                        if (pod_name in self.instance_versions[cluster]):
                            self.logger.info("Double - {}/{}".format(cluster, pod_name))
                        pod_scrub_name = ScrubService(pod_obj['name'])
                        pod_obj["scrubbed_name"] = pod_scrub_name
                        self.instance_versions[cluster][pod_name] = pod_obj
        except:
            self.logger.error(traceback.format_exc())

    def ParseYamls(self):
        self.kube_yamls = Utils.ParseYamls(self.args.cluster)

    def ParseCSV(self):
        (ccp, cluster) = self.args.cluster.split('-')
        self.csv_versions = Utils.ParseCSV('{}_ReleasePlan.csv'.format(cluster))

    def MonitorRollOver(self):
        '''
        The meat of the process.  Watch the cluster for a given amount of time.  Fail if things don't stabilize.
        '''

        time_start = time.time()
        time_end = time_start + self.args.waittime
        stable = False
        fail = False

        self.logger.info("Monitoring instance for proper rollover and version updates")
        self.logger.info("'{}' cluster.  {} allowed restarts.  Polling is {} seconds.  Wait time is {} seconds.".format(self.args.cluster,
                                                                                                                      self.args.restarts,
                                                                                                                      self.args.polltime,
                                                                                                                      self.args.waittime))
        # Gather up our info from the csv manifest, the config yamls
        self.ParseCSV()
        self.ParseYamls()
        while (time.time() < time_end):
            # Update with the instancely data
            self.LoadInstances()
            yaml_keys = self.kube_yamls.keys()
            instance_cluster = self.instance_versions[self.args.cluster]
            instance_keys = instance_cluster.keys()
            csv_keys = self.csv_versions.keys()

            # Cycle through the items in the csv manifest and only check those
            stable = True
            for csv_key in csv_keys:
                # Match only the items found in the csv manifest to the items found in the instancely info
                matches = [ instance_cluster[key] for key in instance_keys if key.find(csv_key) > -1 ]
                for match in matches:
                    # Compare each version of the matched services to the version in the kube yaml files
                    if (match['pkg_version'] != self.kube_yamls[match['scrubbed_name']]):
                        self.logger.error("Version mismatch for service '{}' :  {} != {}  Restarts: {}".format(match['name'], match['pkg_version'], self.kube_yamls[csv_key], match['restarts']))
                        stable = False
                    else:
                        self.logger.info("Match for service '{}' :  {} == {}  Restarts: {}".format(match['name'], match['pkg_version'], self.kube_yamls[match['scrubbed_name']], match['restarts']))
                    # Check restarts
                    if (match['restarts'] > self.args.restarts):
                        self.logger.error("Pod '{}' has restarted more than {} times.".format(match['name'], match['restarts']))
                        fail = True

            if (not stable):
                self.logger.info("Cluster not yet stable.")
            else:
                self.logger.info("Cluster is stable!")
                return True

            if (fail):
                self.logger.error("Cluster pods have rebooted more than {} times.  Exiting".format(self.args.restarts))
                return False

            self.logger.info("Sleeping for {} seconds.  {} seconds left for cluster to settle\n".format(self.args.polltime, int(time_end - time.time())))
            time.sleep(self.args.polltime)

        if (not stable):
            self.logger.error("Cluster versions did not stabilize after {} seconds.".format(self.args.waittime))
            return False

        return True

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Graphite Statistics Generation and Reporting')
    parser.add_argument('--cluster', '-c', type=str, help='Cluster to monitor', required=True)
    parser.add_argument('--waittime', '-w', type=int, help='Seconds to wait for things to stablize', default=600)
    parser.add_argument('--polltime', '-p', type=int, help='Seconds to sleep between polls', default=5)
    parser.add_argument('--restarts', '-r', type=int, help='Number of acceptable restarts', default=10)

    args = parser.parse_args()

    ps = PodStability(args)
    exit( not ps.MonitorRollOver())
