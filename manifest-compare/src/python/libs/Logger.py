import time

ERROR = 3
DEBUG = 2
WARN = 1
INFO = 0

class Logger():
    '''
    A very simple singleton-like logger instance.  Instantiate this normally if you wish to have an instance of your own.
    Otherwise calling Logger.get_instance() will always give you the same instance for easy and consistent logging.
    '''

    LOGGER_INSTANCE = None

    def __init__(self, log_level=INFO, log_filename=None):
        self.log_level = log_level
        self.log_filename = log_filename
        self.out_fp = None
        self.to_stdout = True
        self.to_file = True

        self.open(log_filename)

    def debug(self, msg, log_file=None, log_stdout=None):
        self.log('{} DEBUG: {}'.format(time.ctime(), msg), log_file, log_stdout)

    def info(self, msg, log_file=None, log_stdout=None):
        self.log('{} INFO: {}'.format(time.ctime(), msg), log_file, log_stdout)

    def warn(self, msg, log_file=None, log_stdout=None):
        self.log('{} WARN: {}'.format(time.ctime(), msg), log_file, log_stdout)

    def error(self, msg, log_file=None, log_stdout=None):
        self.log('{} ERROR: {}'.format(time.ctime(), msg), log_file, log_stdout)

    def log(self, msg, log_file=None, log_stdout=None):
        '''
        Log routine to selectivly print to screen and/or a log file

        :param msg: Message to log
        :param log_file: Optional boolean on whether to log to a file
        :param log_stdout: Optional boolean on whether to log to stdout
        :return: msg
        '''
        # Check log to stdout
        if (log_stdout or self.to_stdout):
            print(msg)

        # Check log to file
        if ((log_file or self.to_file) and self.out_fp):
            self.out_fp.write('{}\n'.format(msg))

        return msg

    def open(self, log_filename):
        '''
        If we are passed in a filename for a get_instance call, open it up.

        :param log_filename: filename of the log_file
        '''
        try:
            if (log_filename and not self.out_fp):
                self.log_filename = log_filename
                self.out_fp = open(self.log_filename, 'w')
                print("Opening {} on {}".format(self.log_filename, time.ctime()))
        except:
            print(traceback.format_exc())
            exit(1)

    def close(self):
        '''
        As it says, close the file handle if we have one
        '''
        if (self.out_fp):
            print("Closing {} on {}".format(self.log_filename, time.ctime()))
            self.out_fp.close()

        self.log_filename = None
        self.out_fp = None

    def set_to_stdout(self, to_stdout):
        self.to_stdout = to_stdout

    def set_to_file(self, to_file):
        self.to_file = to_file

    @classmethod
    def get_instance(cls, log_level=INFO, log_filename=None, refresh=False):
        '''
        Bind this function to the class itself.  This way we always get the same cls object and therefore have access to the
        smae LOGGER_INSTANCE class variable across seeparate calls to this method.

        :param log_level: Level of message to log
        :param log_filename: Log filename if we want one
        :param refresh: Whether or not to close and reopen the log file
        '''
        # If we don't have an instance yet, make one
        if (cls.LOGGER_INSTANCE == None):
            cls.LOGGER_INSTANCE = Logger(log_level)

        # Refresh the log file
        if (refresh):
            cls.LOGGER_INSTANCE.close()

        # If we got a filename, open it
        cls.LOGGER_INSTANCE.open(log_filename)

        return cls.LOGGER_INSTANCE
