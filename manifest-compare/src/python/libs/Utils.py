import os
import yaml
import json
import traceback
import csv

def PrintStr(msg, indent=2):
    return json.dumps(msg, indent=indent)

def Print(msg, indent=2):
    print(PrintStr(msg))

def yaml_as_python(filename):
    yaml_list = []
    with open(filename, 'r') as yaml_fp:
        results = yaml.load_all(yaml_fp)

        for value in results:
            yaml_list.append(value)

    return yaml_list

def ParseYamls(cluster_name):
    '''
    Create a list of hashes for service -> version from the yaml files
    '''
    # A path that's relative to jenkins' workspace
    base_cluster_path = 'data/kubernetes_env_configs/{}/config/{}'
    yaml_versions = {}

    print("Reading '{}' services versions yaml files:".format(cluster_name))
    yaml_files = os.listdir(base_cluster_path.format(cluster_name, ''))
    for yaml_file in yaml_files:
        yaml_path = base_cluster_path.format(cluster_name, yaml_file)
        yaml_docs = yaml_as_python(yaml_path)
        for config in yaml_docs:
            try:
                service_name = config['metadata']['name']
                containers = config['spec']['template']['spec']['containers']
                for container in containers:
                    (path, image) = container['image'].split("/")
                    (image_name, version) = image.strip().split(":")
                    print("  {} : {}".format(service_name, version))
                    yaml_versions[service_name] = version
                    break
            except:
                #print(traceback.format_exc())
                pass
    print("Found {} services".format(len(yaml_versions)))
    print('')

    return yaml_versions

def ParseCSV(release_csv):
    '''
    Load the release plan document which is in CSV format.  Uses a 2 state state machine
    '''
    # A path that's relative to jenkins' workspace
    base_release_path = 'data/{}'
    csv_versions = {}

    print("Reading '{}' services versions from csv:".format(release_csv))
    csv_path = base_release_path.format(release_csv)
    with open(csv_path, 'r') as csv_fp:
        csv_doc = csv.reader(csv_fp, delimiter=',', quotechar='"')
        state = 0
        for item in csv_doc:
            # print(len(item[0]))
            if (state == 1):
                if (len(item[1]) > 0):
                    print("  '{}' : '{}' Found".format(item[0], item[1].strip()))
                    csv_versions[item[0]] = item[1].strip()
            if (len(item[0]) == 0):
                if (state == 1):
                    break
                else:
                    state = 0
            if (item[0] == 'Service'):
                state = 1
    print("Found {} services".format(len(csv_versions)))
    print('')

    return csv_versions
