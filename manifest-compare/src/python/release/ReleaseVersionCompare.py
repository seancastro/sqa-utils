#!/usr/bin/python

import sys
sys.path.append('src/python/libs')

import argparse
import yaml

import Utils

class GatherVersions(object):
    '''
    Initialize yaml_version list
    Init the cluster name to load

    :param cluster_name: cluster name to load a list of yaml files from
    '''
    def __init__(self, cluster_name, release_csv):
        self.yaml_versions = {}
        self.csv_versions = {}
        self.cluster_name = cluster_name
        self.release_csv = release_csv

    '''
    Load up all the documents in a single yaml file

    :param filename: YAML file to load from
    :return: YAML dict object
    '''
    def yaml_as_python(self, filename):
        yaml_list = []
        with open(filename, 'r') as yaml_fp:
            results = yaml.load_all(yaml_fp)

            for value in results:
                yaml_list.append(value)

        return yaml_list

    def GrabYamls(self):
        self.yaml_versions = Utils.ParseYamls(self.cluster_name)

    def GrabCSVs(self):
        self.csv_versions = Utils.ParseCSV(self.release_csv)

    def CompareVersions(self):
        '''
        Do the compare against service name and service version
        '''
        hit_list = []
        miss_list = []
        print("Comparing service versions:")
        if (len(self.csv_versions) < 1):
            print("No service versions in csv file.  Assuming this is wrong.")
            return(0)
        if (len(self.yaml_versions) < 1):
            print("No service versions in yaml files.  Assuming this is wrong.")
            return(0)

        for csv_service in self.csv_versions:
            yaml_matches = [ service for service in self.yaml_versions if service.find(csv_service) > -1 ]
            found = False
            for match in yaml_matches:
                if (self.yaml_versions[match] == self.csv_versions[csv_service]):
                    found = True
                    break
            if (found):
                hit_list.append([ csv_service, yaml_matches ])
            else:
                miss_list.append([ csv_service, yaml_matches ])

        print('')
        print("Version match results:")
        for hit in hit_list:
            print("  + Matches: '{}' => '{}' == '{}'".format(hit[0], self.yaml_versions.get(hit[0]), self.csv_versions.get(hit[0])))
        print("{} Matches found".format(len(hit_list)))
        print('')

        for miss in miss_list:
            print("  + No Matches: '{}' => '{}' != '{}'".format(miss[0], self.yaml_versions.get(miss[0]), self.csv_versions.get(miss[0])))
        print("{} Match failures".format(len(miss_list)))
        print('')

        if (len(miss_list) > 0):
            print("-- Errors in version comparisons --")
        else:
            print("++ Everything looks good ++")

        return (len(miss_list) == 0)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Compare release manifest versions against yaml files")
    parser.add_argument('-c', metavar='cluster', help='Name of cluster, i.e., ccp-qa, ccp-qa2',
                        nargs=1, required=True, dest='cluster', choices=['ccp-qa', 'ccp-qa2', 'ccp-cte', 'ccp-uat', 'ccp-prod'])
    parser.add_argument('-r', metavar='release', help='Name of csv release plan',
                        nargs=1, required=True, dest='release', type=str)
    parser.add_argument('-n', metavar='ret', help='Just return this value for testing',
                        nargs=1, required=False, dest='ret', type=int, default=[99])

    args = parser.parse_args()
    if (args.ret[0] != 99):
        print("No action.  Returning exit status {}".format(args.ret[0]))
        exit(args.ret[0])

    gv = GatherVersions(args.cluster[0], args.release[0])
    gv.GrabYamls()
    gv.GrabCSVs()

    exit(not gv.CompareVersions())
