#!/usr/bin/python2.7

import os
import requests
import csv
import json
import traceback

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

PORT = 8000
KEY = 'JAAL6Rg7hb78j476AD'
SECRET = 'FYA8DZ3MWXhx2Ew6JufUq6sFGS8Tn8h8'

base_release_path = './{}'

parse_req = {
    'request' : {
        'method' : None,
        'args' : {}
    },
    'services' : {
        'service' : {
            'name' : None,
            'commits' : [ {
                'c_name' : None,
                'c_author' : None,
                'c_date' : None,
                'c_desc' : None,
                'c_tag' : None,
                'c_branch' : None,
                'files' : []
            } ]
        }
    }
}

class SimpleServer(BaseHTTPRequestHandler):

    def ParseRequest(self, req):
        try:
            if (req.find("?") >= 0):
                (cmd, arg_list) = req.split("?")
                parse_req['request']['method'] = cmd
                arg_items = arg_list.split("&")
                for arg_item in arg_items:
                    (key, value) = arg_item.split('=')
                    parse_req['request']['args'][key] = value
            else:
                parse_req['request']['method'] = req

        except Exception as e:
            return e

        return parse_req

    def LoadCSV(self, req):
        '''
        Read a CSV data file
        '''
        data_file = parse_req['request']['args']['filename']
        csv_path = base_release_path.format(data_file)

        parse_req['services'] = {}

        print("Reading '{}' services versions from csv:".format(csv_path))
        with open(csv_path, 'r') as csv_fp:
            csv_doc = csv.reader(csv_fp, delimiter=',', quotechar='"')
            for item in csv_doc:
                if (not len(item)):
                    return
                service_names = parse_req['services'].keys()
                service_name = item[0]
                if (len(item[4]) > 0):
                    if (item[0] == 'Repo Name'):
                        continue
                service_obj = parse_req['services'].get(service_name)
                if (not service_obj):
                    # print("New service: {}".format(service_name))
                    parse_req['services'][service_name] = {
                        'name' : service_name,
                        'version' : "",
                        'commits' : [] }

                commit = {
                    'c_author' : item[2],
                    'c_date' : item[1],
                    'c_desc' : item[7],
                    'c_tag' : item[4],
                    'c_hash' : item[3],
                    'c_branch' : item[5],
                    'files' : item[6]
                }
                service = parse_req['services'][service_name]
                commit_obj = service.get('commits')
                service['commits'].append(commit)

                if (item[4]):
                    if (item[4] > service['version']):
                        service['version'] = item[4]

    def do_GET(self):
        try:
            self.ParseRequest(self.path)
            self.LoadCSV(self.path)

            if (parse_req['request']['method'] == '/services'):
                print("Services")
                args = parse_req['request']['args']
                if (not 'filename' in args):
                    raise Exception("/services endpoint needs ?filename=<filename>")
                self.send_response(200)
                self.send_header('Content-type', 'application/json')
                self.send_header('Access-Control-Allow-Origin', '*')
                self.end_headers()

                services = parse_req['services']
                context = args.get('context')
                if (not context):
                    self.wfile.write(json.dumps(parse_req, indent=2))
                    return
                elif (context == 'all'):
                    print(json.dumps(parse_req, indent=2))
                    self.wfile.write(json.dumps(parse_req, indent=2))
                    return
                elif (context == 'tagged'):
                    tagged = [ service for service in parse_req['services'] if (len(parse_req['services'][service]['version']) > 0) ]
                    tagged_services = {}
                    for item in tagged:
                        tagged_services[item] = services[item]
                    self.wfile.write(json.dumps({ 'services' : tagged_services}, indent=2))
                    return
                elif (context == 'notags'):
                    services = parse_req['services']
                    not_tagged = [ service for service in parse_req['services'] if (len(services[service]['version']) == 0) ]
                    nontagged_services = {}
                    for item in not_tagged:
                        nontagged_services[item] = services[item]
                    self.wfile.write(json.dumps({ 'services' : nontagged_services}, indent=2))
                    return

            print("No End Point")
            self.send_response(500)
            self.send_header('Content-type', 'application/json')
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            self.wfile.write("API Endpoint does not exist: '{}'\n\n".format(self.path))
            self.wfile.write("Valid existing end points:\n")
            self.wfile.write("'/services?filename=<filename>&context=<all|tags>' -- Gather a list of services.\n")
            return
        except Exception as e:
            self.send_response(500)
            self.send_header('Content-type', 'application/json')
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            self.wfile.write("Error while processsing request:\n")
            self.wfile.write("Exception: {}".format(traceback.format_exc()))
            print("Exception: {}".format(traceback.format_exc()))
            return

    def get_commits(service, size=10):
        msg = ""
        services_commits = {}
        cmd_tmp = 'git clone git@bitbucket.org:clearcapital/{}.git'.format(service)
        cmd_resp = (os.popen(cmd_tmp)).read()
        cmd_resp = run_cmd('cd {}; git log -{}; cd ..'.format(service, size))
        log_list = cmd_resp.split('\n')
        services_commits[service] = []
        commit = {}
        state = 0
        for line in log_list:
            # print(line)
            if (state == 0):
                if (line.find('commit') == 0):
                    (cmd, commit_hash) = line.split(' ')
                    commit['c_hash'] = commit_hash.strip()
                if (line.find('Author') == 0):
                    (cmd, author) = line.split(':')
                    commit['c_author'] = author.strip()
                if (line.find('Date') == 0):
                    (cmd, date_stamp) = line.split(':', 1)
                    commit['c_date'] = date_stamp.strip()
                if (len(line) == 0):
                    state = 1
            elif (state == 1):
                if (len(line) > 0):
                    if (not 'desc' in commit):
                        commit['c_desc'] = str(line.strip())
                    else:
                        commit['c_desc'] += str(line.strip())
                else:
                    state = 0
                    services_commits[service].append(commit)
                    commit = {}

        return services_commits

httpd = HTTPServer(("", PORT), SimpleServer)
print("Serving port {}".format(PORT))
httpd.serve_forever()
